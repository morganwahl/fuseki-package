UPSTREAM_VERSION=3.13.1
PACKAGE_VERSION=1
P=fuseki_${UPSTREAM_VERSION}-${PACKAGE_VERSION}

deb: ${P}.deb

${P}.deb: ${P}/opt/fuseki ${P}/lib/systemd/system/fuseki.service ${P}/DEBIAN/control ${P}/DEBIAN/postinst ${P}/DEBIAN/prerm ${P}/etc/fuseki
	fakeroot dpkg-deb --build ${P}

${P}/opt/fuseki: apache-jena-fuseki-${UPSTREAM_VERSION}.tar.gz
	mkdir -p ${P}/opt/fuseki
	tar xf apache-jena-fuseki-${UPSTREAM_VERSION}.tar.gz -C ${P}/opt/fuseki/ --strip-components=1

${P}/etc/fuseki:
	mkdir -p ${P}/etc/fuseki

${P}/lib/systemd/system/fuseki.service: ${P}/opt/fuseki
	mkdir -p ${P}/lib/systemd/system
	cp ${P}/opt/fuseki/fuseki.service $@

${P}/DEBIAN/%: DEBIAN-${PACKAGE_VERSION}/%
	mkdir -p ${P}/DEBIAN
	cp -v $< $@

apache-jena-fuseki-${UPSTREAM_VERSION}.tar.gz:
	wget --no-verbose http://mirrors.sonic.net/apache/jena/binaries/apache-jena-fuseki-${UPSTREAM_VERSION}.tar.gz

clean:
	rm -frv fuseki_*.deb
	rm -frv fuseki_*-*
